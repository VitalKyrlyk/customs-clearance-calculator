package uzh.customsclearancecalculator;

public class MainActivityPresenter {

    private static MainActivityPresenter instance;
    private MainActivityView activityView;

    private MainActivityPresenter(MainActivityView activityView){
        this.activityView = activityView;
    }

    public static synchronized MainActivityPresenter getInstance(MainActivityView activityView){
        if (instance == null){
            instance = new MainActivityPresenter(activityView);
        }
        return instance;
    }

    public void calculate(int priceCar, double engineCapacity, String typeEngine, int ageCar){
        int baseRate;
        if (typeEngine.equals("Бензин")){
            if (engineCapacity <= 3){
                baseRate = 50;
            } else {
                baseRate = 100;
            }
        } else {
            if (engineCapacity <= 3.5){
                baseRate = 75;
            } else {
                baseRate = 150;
            }
        }

        double rate = baseRate * engineCapacity * ageCar;
        double saleRate = rate * 0.5;

        double toll = priceCar * 0.1;
        double pdv = (priceCar + saleRate + toll) * 0.2;

        double priceOfCustomsClearance = saleRate + toll + pdv;

        activityView.getPriceOfCustomsClearance(priceOfCustomsClearance, saleRate, pdv, toll);
    }
}
