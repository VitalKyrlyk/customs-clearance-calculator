package uzh.customsclearancecalculator;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.ManagerFactoryParameters;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MainActivityView {

    int ageCar;
    String typeEgine;
    MainActivityPresenter presenter;

    @BindView(R.id.priceCar)
    EditText priceCar;

    @BindView(R.id.engineCapacity)
    EditText engineCapacity;

    @BindView(R.id.spinnerType)
    Spinner spinnerType;

    @BindView(R.id.spinnerAge)
    Spinner spinnerAge;

    @BindView(R.id.priceOfCustomsClearance)
    TextView tvPriceOfCustomsClearance;

    @BindView(R.id.saleRate)
    TextView tvSaleRate;

    @BindView(R.id.pdv)
    TextView tvPdv;

    @BindView(R.id.toll)
    TextView tvToll;

    @OnClick(R.id.btnCalculate)
    void Calculate(){
        presenter.calculate(Integer.parseInt(priceCar.getText().toString()),
                Integer.parseInt(engineCapacity.getText().toString()),
                        typeEgine, ageCar);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = MainActivityPresenter.getInstance(this);

        setAdapters();

    }

    void setAdapters(){
        final List<Integer> listAge = new ArrayList<>();
        for (int i = 1; i < 16; i++){
            listAge.add(i);
        }

        ArrayAdapter<Integer> arrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_item, listAge);
        spinnerAge.setAdapter(arrayAdapter);
        spinnerAge.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ageCar = listAge.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        final List<String> listType = new ArrayList<>();
        listType.add("Бензин");
        listType.add("Дизель");

        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<>(this, R.layout.simple_spinner_item, listType);
        spinnerType.setAdapter(stringArrayAdapter);
        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                typeEgine = listType.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void getPriceOfCustomsClearance(double priceOfCustomsClearance,
                                           double saleRate, double pdv, double toll) {
        tvPriceOfCustomsClearance.setText("Всього митних платежів "
                + String.valueOf(priceOfCustomsClearance));
        tvSaleRate.setText(String.valueOf("Акцизний збір " + saleRate));
        tvPdv.setText(String.valueOf("ПДВ " + pdv));
        tvToll.setText(String.valueOf("Мито " + toll));
    }

}
