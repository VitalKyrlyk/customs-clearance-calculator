package uzh.customsclearancecalculator;

public interface MainActivityView {

    void getPriceOfCustomsClearance(double priceOfCustomsClearance, double saleRate,
                                    double pdv, double toll);
}
